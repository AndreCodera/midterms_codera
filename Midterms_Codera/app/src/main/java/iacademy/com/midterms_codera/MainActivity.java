package iacademy.com.midterms_codera;

import android.support.annotation.DrawableRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    String text = "I\'m so full";
    String text2 = "I\'m so hungry";
    int quant = 1;
    boolean checked = false;
    int price = 180;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.midterms_forms);
    }

    public void eatCookie(View view){
        ImageView img = (ImageView)findViewById(R.id.cookieimg);
        img.setImageResource(R.drawable.after_cookie);

        TextView txt = (TextView)findViewById(R.id.cookietext);
        txt.setText(text);
    }

    public void reset(View view){
        ImageView img = (ImageView)findViewById(R.id.cookieimg);
        img.setImageResource(R.drawable.before_cookie);

        TextView txt = (TextView)findViewById(R.id.cookietext);
        txt.setText(text2);
    }

    public void displayQuant(int n){
        TextView num = (TextView)findViewById(R.id.num);
        num.setText("" + n);
    }

    public void increment(View view){
        quant++;
        displayQuant(quant);
    }
    public void decrement(View view){
        quant--;
        displayQuant(quant);
    }

    public boolean isWhippedCreamChecked(){
        CheckBox chck = (CheckBox)findViewById(R.id.checkBox);
        checked = chck.isChecked();
        return checked;
    }

    public boolean isChocolateChecked(){
        CheckBox chck = (CheckBox)findViewById(R.id.checkBox2);
        checked = chck.isChecked();
        return checked;
    }

    public int calculatePrice(boolean b1, boolean b2){
        if (!b1 && b2){
            price *= quant;
            price += 50;

        }
        if (b1 && !b2){
            price *= quant;
            price += 40;
        }
        if (b1 && b2) {
            price *= quant;
            price += 50;
        }
        if (!b1 && !b2) {
            price *= quant;
        }

        return price;
    }

    public String createOrderSummary(int price, boolean addWC, boolean addCh){
        String summ = "Name: Andre Codera" + "\nAdd whipped cream?" + "" + addWC + "\nAdd Chocolate?" + "" + addCh + "\nQuantity: " + "" + quant + "Total: " + "" + price;
        return summ;
    }

    public void displayMessage(String mess){
        TextView txt = (TextView)findViewById(R.id.textView5);
        txt.setText(mess);
    }

    public void submitOrder(View view){
        int price = calculatePrice(isWhippedCreamChecked(), isChocolateChecked());
        String message = createOrderSummary(price, isChocolateChecked(), isWhippedCreamChecked());
        displayMessage(message);
    }

    public void reset(){
        displayQuant(1);
        displayMessage("180");
    }
}
